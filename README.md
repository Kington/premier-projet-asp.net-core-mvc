Pour la création du premier projet ASP.NET Core MVC, voir la documentation sur [la page **_Création d'un premier projet ASP.NET Core MVC_** du Wiki](https://gitlab.com/CoursDali/420-4p3-hu-d-veloppement-web-en-asp.net/s1-premier-projet-gabarit/premier-projet-asp.net-core-mvc/-/wikis/Cr%C3%A9ation-d'un-premier-projet-ASP.NET-Core-MVC).


Pour déposer son projet dans GitLab, voir la documentation sur [la page **_Dépôt du projet ASP.NET dans GitLab_**](https://gitlab.com/CoursDali/420-4p3-hu-d-veloppement-web-en-asp.net/s1-premier-projet-gabarit/premier-projet-asp.net-core-mvc/-/wikis/D%C3%A9p%C3%B4t-du-projet-ASP.NET-dans-GitLab) du Wiki.

